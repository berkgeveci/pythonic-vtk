from filters import *

from vtkmodules.vtkImagingCore import vtkRTAnalyticSource
from vtkmodules.vtkFiltersCore import vtkContourFilter, vtkExtractEdges, vtkAppendPolyData
from vtkmodules.vtkFiltersGeneral import vtkShrinkPolyData
from datamodel import vtkDataObject, vtkImageData, vtkPartitionedDataSet
import numpy

if __name__ == "__main__":
    # Create and execute a wavelet source. Here ug is the output.
    ug = vtkRTAnalyticSource(whole_extent=(0, 20, 0, 20, 0, 30), center=(1.0, 1.0, 1.0), maximum=100).execute()
    # Now contor ug.
    ct = vtkContourFilter(contour_values = [30, 40]).execute(ug)
    print(ct.GetNumberOfCells())

    # Create an image and contour
    id = vtkImageData(spacing=(0.1, 0.1, 0.1), origin=(2,2,2))
    id.dimensions = (10, 10, 10)
    cs = numpy.linspace(2, 2 + 9 * 0.1, 10)
    x, y, z = numpy.meshgrid(cs, cs, cs)
    values = 30.0*numpy.sin(x*3)*numpy.cos(y*3)*numpy.cos(z)
    id.point_data['scalar'] = values.flatten(order='F')
    ct = vtkContourFilter(contour_values=[-10, 0, 10], scalar_name='scalar').execute(id)
    print(ct.GetNumberOfCells())

    # Create a partitioned dataset and contour
    pds = vtkPartitionedDataSet()
    for i in range(5):
        dim = i*3+1
        image = vtkImageData(dimensions=(dim, dim, dim))
        d = numpy.ones(image.GetNumberOfPoints())*dim
        image.point_data['scalar'] = d
        pds.append(image)
    pds.append(id)
    for img in pds:
        print(img.dimensions)

    ct = vtkContourFilter(contour_values=[3, 6, 9], scalar_name='scalar').execute(pds)
    for img in ct:
        print(">>", img.GetNumberOfCells())

    o = (vtkRTAnalyticSource(maximum=100) >>
         vtkContourFilter(contour_values=[30, 40])).execute()
    print(o)

    ct = (id >>
          vtkContourFilter(
              contour_values=[-10, 0, 10], scalar_name='scalar')).execute()
    print(ct)

#    print(ct.polygons)
#    o = (OutputPort(vtkRTAnalyticSource(maximum=100), 0) >>
#         vtkContourFilter(contour_values=[30, 40])).execute()
#    print(o.number_of_cells)

    edges = vtkContourFilter() >> vtkShrinkPolyData() >> vtkExtractEdges()
    shrink = vtkContourFilter() >> vtkExtractEdges() >> vtkShrinkPolyData()
    a = vtkAppendPolyData()
    a.AddInputConnection(edges.GetOutputPort())
    a.AddInputConnection(shrink.GetOutputPort())
    print(Pipeline(a).algorithms)
    print(Pipeline(a).input_port)
    print(Pipeline(edges).input_port)

