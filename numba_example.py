from datamodel import vtkUnstructuredGrid
from prog_filter import MySource
import numpy as np
from numba import njit

image = MySource(dimensions=(100, 100, 100)).execute()
uns = image.convert_to_unstructured_grid()
cells = uns.cells
pts = uns.points

@njit(parallel=False, cache=True)
def shrink(pts, offsets, connectivity, new_pts, new_offsets, new_connectivity):
    ncells = offsets.shape[0] - 1
    n_newpts = ncells*8
    for i in range(ncells):
        nverts = offsets[i + 1] - offsets[i]
        conns = connectivity[offsets[i]:offsets[i + 1]]
        cpts = pts[conns, :]
        sum = np.zeros(3, dtype=np.float64)
        for j in range(nverts):
            sum += cpts[j]
        avg = sum / nverts
        new_offsets[i] = i*8
        for j in range(nverts):
            vec = avg - cpts[j]
            vec *= 0.5
            newpt = cpts[j] + vec
            new_pts[i*8+j] = newpt
            new_connectivity[i*8+j] = i*8+j
            
    new_offsets[ncells] = ncells*8+1

offsets = cells['offsets']
connectivity = cells['connectivity']
ncells = offsets.shape[0] - 1
n_newpts = ncells*8
new_pts = np.empty((n_newpts,3), dtype=np.float64)
new_connectivity = np.empty(ncells*8, dtype=np.int64)
new_offsets = np.empty(ncells + 1, dtype=np.int64)
shrink(pts, offsets, connectivity, new_pts, new_offsets, new_connectivity)

grid = vtkUnstructuredGrid()
grid.points = new_pts
grid.cells = { 'offsets': new_offsets, 'connectivity': new_connectivity,
               'cell_types' : cells['cell_types']}

print(uns.GetNumberOfCells(), grid.GetNumberOfCells())

# from vtkmodules.vtkFiltersGeometry import vtkGeometryFilter
# gf = vtkGeometryFilter()
# gf.SetInputData(grid)
# gf.Update()
# import vedo
# m = vedo.Mesh(grid)
# p = vedo.Plotter()
# p.show(m).close()
