from filters import vtkRTAnalyticSource, vtkContourFilter
import pyvista as pv

o = (vtkRTAnalyticSource(maximum=100) >> vtkContourFilter(contours=(30, 40))).Execute()
rep = pv.PolyData(o).plot()

print(type(pv.PolyData(o).point_data))
