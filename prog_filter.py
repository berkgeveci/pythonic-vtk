from filters import AlgorithmBase, vtkContourFilter, vtkShrinkPolyData
from vtk.util.vtkAlgorithm import VTKPythonAlgorithmBase
from vtkmodules.vtkCommonDataModel import vtkDataSet, vtkPolyData, vtkImageData
from vtkmodules.vtkCommonExecutionModel import vtkStreamingDemandDrivenPipeline as vtkSDDP
import numpy as np

class MySource(VTKPythonAlgorithmBase, AlgorithmBase):
    def __init__(self, dimensions=[10, 10, 10]):
        VTKPythonAlgorithmBase.__init__(self, nInputPorts=0,
                                        outputType='vtkImageData')
        self._dimensions = None
        self.dimensions = dimensions

    @property
    def dimensions(self):
        return self._dimensions

    @dimensions.setter
    def dimensions(self, value):
        if value != self._dimensions:
            self._dimensions = value
            self.Modified()

    def RequestInformation(self, request, inInfo, outInfo):
        extent = (0, self._dimensions[0], 0, self._dimensions[1], 0, self._dimensions[2])
        outInfo.GetInformationObject(0).Set(
            vtkSDDP.WHOLE_EXTENT(), extent, 6)
        return 1

    def RequestData(self, request, inInfo, outInfo):
        opt = vtkImageData.GetData(outInfo)
        opt.spacing = (0.1, 0.1, 0.1)
        opt.origin = (2, 2, 2)
        opt.dimensions = self._dimensions
        cs = np.linspace(2, 2 + 9 * 0.1, 10)
        x, y, z = np.meshgrid(cs, cs, cs)
        values = 30.0*np.sin(x*3)*np.cos(y*3)*np.cos(z)
        opt.point_data['scalar'] = values.flatten(order='F')
        return 1


class ContourShrink(VTKPythonAlgorithmBase, AlgorithmBase):
    def __init__(self, contour_values=[], scalar_name=''):
        VTKPythonAlgorithmBase.__init__(self)
        self._contour_values = None
        self.contour_values = contour_values
        self._scalar_name = None
        self.scalar_name = scalar_name

    @property
    def contour_values(self):
        return self._contour_values

    @contour_values.setter
    def contour_values(self, value):
        if value != self._contour_values:
            self._contour_values = value
            self.Modified()

    @property
    def scalar_name(self):
        return self._scalar_name

    @scalar_name.setter
    def scalar_name(self, value):
        if value != self._scalar_name:
            self._scalar_name = value
            self.Modified()

    def RequestData(self, request, inInfo, outInfo):
        inp = vtkDataSet.GetData(inInfo[0])
        opt = vtkPolyData.GetData(outInfo)

        cf = vtkContourFilter(contour_values=self.contour_values, scalar_name='scalar')
        sf = vtkShrinkPolyData()
        opt.ShallowCopy((inp | cf | sf).Execute())

        return 1

if __name__ == "__main__":
    image = MySource().Execute()

    print(ContourShrink(contour_values=(-10, 0, 10),
                        scalar_name='scalar').Execute(image).GetNumberOfCells())

    pd = (image | ContourShrink(contour_values=(-10, 0, 10), scalar_name='scalar')).Execute()
    print(pd.GetNumberOfCells())


