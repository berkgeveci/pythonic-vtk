from filters import RTAnalyticSource, Contour
from vedo import Mesh, Plotter

o = (RTAnalyticSource(maximum=100) >> Contour(contours=(30, 40))).execute()
m = Mesh(o)

plt = Plotter()
plt.show(m, axes=1).close()
