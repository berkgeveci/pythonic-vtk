"""This module provides shim classes for VTK algorithms
that are more python friendly. This is a prototype for
demonstration purposes only. It is not feasible to override
every VTK filter this way and this functionality has to
be done at the C++ or wrapper level to be feasible.
"""

from vtkmodules.vtkCommonExecutionModel import vtkAlgorithmOutput, vtkTrivialProducer

class SelectPorts(object):
    def __init__(self, alg, input_port=0, output_port=0):
        self.algorithm = alg
        self.input_port = input_port
        self.output_port = output_port

    def __rshift__(self, other):
        other.SetInputConnection(self.algorithm.GetOutputPort(self.output_port))
        return other

    def SetInputConnection(self, other):
        self.algorithm.SetInputConnection(self.input_port, other)

    def SetInputDataObject(self, dobj):
        self.algorithm.SetInputDataObject(self.input_port, dobj)

    def execute(self, input_data=None):
        if input_data:
            self.algorithm.execute(input_data)
        else:
            self.algorithm.execute()
        return self.algorithm.GetOutputDataObject(self.output_port)

class Append(object):
    def __init__(self, alg, input_port=0):
        self.algorithm = alg
        self.input_port = input_port

    def GetOutputPort(self, port=0):
        return self.algorithm.GetOutputPort(port)

    def SetInputConnection(self, other):
        self.algorithm.AddInputConnection(self.input_port, other)

    def __rshift__(self, other):
        other.SetInputConnection(self.algorithm.GetOutputPort())

    def execute(self, input_data=None):
        if input_data:
            return self.algorithm.execute(input_data)
        else:
            return self.algorithm.execute()


class Pipeline(object):
    def __init__(self, alg, input_port=None):
        self.input_port = input_port
        self.algorithms = [alg]
        to_process = [alg]
        while len(to_process):
            alg = to_process.pop()
            inputs = []
            for i in range(alg.GetNumberOfInputPorts()):
                for j in range(alg.GetNumberOfInputConnections(i)):
                    ao = alg.GetInputConnection(i, j)
                    inputs.append(ao.GetProducer())
            to_process += inputs
            self.algorithms += inputs
        unique_list = []
        [unique_list.append(x) for x in self.algorithms if x not in unique_list]
        self.algorithms = unique_list
        if not input_port:
            possible_inputs = []
            for alg in self.algorithms:
                if alg.GetNumberOfInputPorts() and not alg.GetInputConnection(0, 0):
                    possible_inputs.append(alg)
            if len(possible_inputs) == 1:
                self.input_port = possible_inputs[0]

    def execute(self, input_data=None):
        if input_data:
            if not self.input_port:
                raise RuntimeError("Trying to assign an input to a pipeline without an input port.")
            tp = vtkTrivialProducer(output=input_data)
            self.input_port.SetInputConnection(tp.GetOutputPort())
        output = self.algorithms[0].execute()
        if input_data and self.input_port:
            self.input_port.SetInputConnection(None)
        return output
