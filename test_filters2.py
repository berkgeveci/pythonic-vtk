from filters import *

from vtkmodules.vtkImagingCore import vtkRTAnalyticSource as Wavelet
from vtkmodules.vtkFiltersCore import vtkContourFilter as Contour, vtkExtractEdges, vtkAppendPolyData as AppendPoly, vtkExtractCells as ExtractCells
from vtkmodules.vtkFiltersGeneral import vtkTableBasedClipDataSet as Clip
from vtkmodules.vtkFiltersGeneral import vtkShrinkPolyData
from vtkmodules.vtkFiltersGeometry import vtkDataSetSurfaceFilter as ExtractSurface

w = Wavelet()

# Using a filter without a pipeline in a more imperative way.
cpoly = Contour(input_data=w.execute(), contour_values=[100, 200])

# Basic use of >> for creating and executing pipelines.
# Note that >> returns its rhs operand.
c = Contour(contour_values=[100, 120])
w >> c
a = AppendPoly()
# The Append class enables connecting multiple inputs to
# a repeatable input such as the input of append filters.
c >> Append(a)
print((c >> Append(a)).execute().number_of_polys)

# Basic use of a Pipeline object to create and store
# a pipeline that can process data. Note that the execute()
# method of Pipeline can take input data.
p=Pipeline(Contour(contour_values=[100]) >> vtkShrinkPolyData())
print(p.execute(w.execute()).number_of_polys)

# Demonstrates the use of the SelectPorts class to determine
# which input and output ports will be used when making connections.
# Here, we connect the two outputs of the clip filter to two
# different pipelines and then append them together. Note that
# VTK has inherent issues with diamond pipelines like this so
# this is for demonstration only.
clip = Clip(value=100, generate_clipped_output=True)
append = AppendPoly()
w >> SelectPorts(clip, output_port=0) >> Contour(contour_values=[200]) >> append
SelectPorts(clip, output_port=1) >> ExtractSurface() >> Append(append)
p = Pipeline(append)
print(p.algorithms)
print(p.input_port)
print(p.execute())

# Similar to the previous example except that the pipeline we created
# has an input port which can be used to process arbitrary inputs.
clip = Clip(value=100, generate_clipped_output=True)
append = AppendPoly()
SelectPorts(clip, output_port=0) >> Contour(contour_values=[200]) >> append
SelectPorts(clip, output_port=1) >> ExtractSurface() >> Append(append)
p = Pipeline(append)
p.execute(w.execute())
